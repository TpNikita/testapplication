package com.example.testapplication.screen;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.testapplication.R;
import com.example.testapplication.base.BaseFragment;
import com.example.testapplication.domain.model.Shop;
import com.example.testapplication.screen.adapters.RecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public class MainFragment extends BaseFragment implements MainView {

    private Unbinder unbinder;
    private RecyclerViewAdapter adapter;
    private ArrayList<Shop> shops = new ArrayList<>();

    @InjectPresenter
    MainPresenter presenter;

    public static com.example.testapplication.screen.MainFragment newInstance() {
        Bundle args = new Bundle();
        com.example.testapplication.screen.MainFragment fragment = new com.example.testapplication.screen.MainFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_main, container, false);
        unbinder = ButterKnife.bind(this, v);
        RecyclerView recyclerView = (RecyclerView) v.findViewById(R.id.recyclerView);
        adapter = new RecyclerViewAdapter(shops);
        LinearLayoutManager layoutManager = new LinearLayoutManager(v.getContext());
        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(itemAnimator);
        presenter.downloadData();
        return v;
    }

    @Override
    protected Integer getLayoutId() {
        return R.layout.fragment_main;
    }


    @Override
    public void showLoading(boolean loading) {

    }

    @Override
    public void successLoad(@NonNull List<Shop> shops) {
        adapter.setShops(shops);
        adapter.notifyDataSetChanged();
    }
}