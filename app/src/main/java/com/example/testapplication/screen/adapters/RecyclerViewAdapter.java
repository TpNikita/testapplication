package com.example.testapplication.screen.adapters;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.testapplication.R;
import com.example.testapplication.domain.model.Shop;
import com.example.testapplication.util.ViewUtil;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private List<Shop> shops = Collections.emptyList();

    public void setShops(List<Shop> shops) {
        this.shops = shops;
    }

    public List<Shop> getShops() {
        return shops;
    }

    public RecyclerViewAdapter(List<Shop> shops) {
        setShops(shops);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.itemName.setText(shops.get(position).getName());
        holder.price.setText("$" + String.valueOf(shops.get(position).getPrice()));
        if (URLUtil.isNetworkUrl(shops.get(position).getImageURL())) {
            ViewUtil.picassoLoadImage(shops.get(position).getImageURL(), holder.image);
        }
        holder.distance.setText(shops.get(position).getDistance() + " km");
        holder.cpb.setProgress(shops.get(position).getShopRank()/10);
        if (URLUtil.isNetworkUrl(shops.get(position).getShopLogo())) {
            ViewUtil.picassoLoadImage(shops.get(position).getShopLogo(), holder.imageLogo);
        }
    }

    @Override
    public int getItemCount() {
        return shops.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_name)
        TextView itemName;

        @BindView(R.id.price)
        TextView price;

        @BindView(R.id.image)
        ImageView image;

        @BindView(R.id.image_logo)
        ImageView imageLogo;

        @BindView(R.id.distance)
        TextView distance;

        @BindView(R.id.progressBar1)
        CircularProgressBar cpb;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
