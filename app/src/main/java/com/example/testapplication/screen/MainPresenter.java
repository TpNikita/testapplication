package com.example.testapplication.screen;


import com.arellomobile.mvp.InjectViewState;
import com.example.testapplication.base.presenter.BasePresenter;
import com.example.testapplication.data.network.RestClient;
import com.example.testapplication.data.network.model.Message;
import com.example.testapplication.data.network.model.MyResponse;
import com.example.testapplication.domain.data.DataUseCase;

import io.reactivex.android.schedulers.AndroidSchedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@InjectViewState
public class MainPresenter extends BasePresenter<MainView> {
    private DataUseCase dateUseCase = new DataUseCase();

    public void downloadData() {
        addDisposable(dateUseCase.run(new DataUseCase.Params())
                .observeOn(AndroidSchedulers.mainThread())
                .onTerminateDetach()
                .doOnSubscribe(disposable -> getViewState().showLoading(true))
                .doOnTerminate(() -> getViewState().showLoading(false))
                .subscribe(result -> getViewState().successLoad(result.getShops())));
    }
}
