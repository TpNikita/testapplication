package com.example.testapplication.screen;


import android.support.annotation.NonNull;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.example.testapplication.base.view.CloseableView;
import com.example.testapplication.base.view.LoadingView;
import com.example.testapplication.domain.model.Shop;

import java.util.List;

public interface MainView extends MvpView,LoadingView, CloseableView {
    @StateStrategyType(OneExecutionStateStrategy.class)
    void successLoad(@NonNull List<Shop> shops);
}
