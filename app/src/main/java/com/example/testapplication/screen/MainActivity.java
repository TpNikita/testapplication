package com.example.testapplication.screen;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.testapplication.R;
import com.example.testapplication.base.BaseActivity;


public class MainActivity extends BaseActivity {



    @Nullable
    @Override
    protected Fragment getContentFragment() {
        return MainFragment.newInstance();
    }
}
