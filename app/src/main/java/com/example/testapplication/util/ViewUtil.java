package com.example.testapplication.util;


import android.support.annotation.NonNull;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class ViewUtil {
    public static void picassoLoadImage(@NonNull String url, @NonNull ImageView imageView) {
        Picasso.with(imageView.getContext())
                .load(url)
                .fit()
                .centerCrop()
                .into(imageView);
    }
}
