package com.example.testapplication.data.network;


import com.example.testapplication.data.network.model.MyResponse;

import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;

public interface ServerAPI {
    String HOST = "https://api.jeench.com/";

    @GET("v1/search-items")
    @Headers({"Content-Type: application/xml", "Accept: application/xml"})
    Single<MyResponse> downloadData();
}
