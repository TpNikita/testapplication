package com.example.testapplication.data.network.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

import java.util.List;


@Root(name = "message")
public class Message {


    public Message() {
    }

    public List<ShopResult> getItems() {
        return lsitItem;
    }

    public void setItems(List<ShopResult> sr) {
        this.lsitItem = sr;
    }

    public Message(List<ShopResult> sr) {
        this.lsitItem = sr;
    }

    //@Path("response/message/item")
    @ElementList(entry = "item",inline = true)
    List<ShopResult> lsitItem;
}
