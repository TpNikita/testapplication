package com.example.testapplication.data.network.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;


@Root(name = "response")
public class MyResponse {
    public MyResponse() {
    }

    public MyResponse(String code, Message message) {
        this.code = code;
        this.message = message;
    }


    @Element(name = "message")
    Message message;


    @Element(name = "code")
    private String code;

    public String getCode() {

        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Message getMessage() {
        return message;
    }
}
