package com.example.testapplication.domain.mapping;


public abstract class Mapping<From, To> {
    public abstract To map(From from);
}
