package com.example.testapplication.domain.model;


public class Shop {
    public Shop(String shopLogo, float shopRank, String imageURL, String name, float price, int distance) {
        this.shopLogo = shopLogo;
        this.shopRank = shopRank;
        this.imageURL = imageURL;
        this.name = name;
        this.price = price;
        this.distance = distance;
    }

    private String shopLogo;

    public String getShopLogo() {
        return shopLogo;
    }

    public float getShopRank() {
        return shopRank;
    }

    public String getImageURL() {
        return imageURL;
    }

    public String getName() {
        return name;
    }

    public float getPrice() {
        return price;
    }

    public int getDistance() {
        return distance;
    }

    private float shopRank;
    private String imageURL;
    private String name;
    private float price;
    private int distance;

}
