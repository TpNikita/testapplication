package com.example.testapplication.domain.data;


import android.support.annotation.NonNull;

import com.example.testapplication.data.network.RestClient;
import com.example.testapplication.domain.UseCase;
import com.example.testapplication.domain.mapping.ItemListToShopListMapper;
import com.example.testapplication.domain.model.Shop;

import java.util.List;

import io.reactivex.Observable;

public class DataUseCase extends UseCase<DataUseCase.Params, DataUseCase.Result> {

    private final ItemListToShopListMapper mapperList = new ItemListToShopListMapper();

    @Override
    public Observable<Result> run(@NonNull Params params) {
        return RestClient.getInstance().downloadData().doOnSuccess(r -> {
        })
                .map(mapperList::map)
                .map(result -> new Result(result))
                .toObservable();
    }

    public static class Params extends UseCase.Params {


    }


    public static class Result extends UseCase.Result {
        private List<Shop> shops;

        public Result(List<Shop> shops) {
            this.shops = shops;
        }

        public List<Shop> getShops() {
            return shops;
        }
    }
}
