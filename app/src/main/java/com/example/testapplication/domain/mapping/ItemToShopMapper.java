package com.example.testapplication.domain.mapping;


import com.example.testapplication.data.network.model.ShopResult;
import com.example.testapplication.domain.model.Shop;

public class ItemToShopMapper extends Mapping<ShopResult, Shop> {
    @Override
    public Shop map(ShopResult shopResult) {
        return new Shop(
                shopResult.getShop_logo(),
                Float.valueOf(shopResult.getShop_rank()),
                shopResult.getItem_image(),
                shopResult.getItem_name(),
                Float.valueOf(shopResult.getItem_price()),
                Integer.parseInt(shopResult.getPoint_distance())
        );
    }
}
