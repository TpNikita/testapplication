package com.example.testapplication.domain.mapping;


import com.example.testapplication.data.network.model.MyResponse;
import com.example.testapplication.data.network.model.ShopResult;
import com.example.testapplication.domain.model.Shop;

import java.util.ArrayList;
import java.util.List;

public class ItemListToShopListMapper extends Mapping<MyResponse, List<Shop>> {
    private ItemToShopMapper mapper = new ItemToShopMapper();

    @Override
    public List<Shop> map(MyResponse myResponse) {
        ArrayList<Shop> shops = new ArrayList<>(myResponse.getMessage().getItems().size());
        for (int i = 0; i < myResponse.getMessage().getItems().size(); i++) {
            shops.add(mapper.map(myResponse.getMessage().getItems().get(i)));
        }
        return shops;
    }
}
