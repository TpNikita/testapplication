package com.example.testapplication.base.view;


import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

public interface CloseableView extends MvpView {
    @StateStrategyType(OneExecutionStateStrategy.class)
    void closeView();
}
