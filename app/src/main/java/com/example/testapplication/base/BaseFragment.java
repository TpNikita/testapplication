package com.example.testapplication.base;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.example.testapplication.base.view.CloseableView;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;


public class BaseFragment extends MvpAppCompatFragment implements CloseableView {
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Integer layout = getLayoutId();
        if (layout == null) {
            return super.onCreateView(inflater, container, savedInstanceState);
        } else {
            return inflater.inflate(layout, container, false);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        compositeDisposable.clear();
    }

    @Nullable
    @LayoutRes
    protected Integer getLayoutId() {
        return null;
    }


    @Override
    public void closeView() {
        Activity activity = getActivity();
        if (activity != null) {
            activity.onBackPressed();
        }
    }

    protected void addDisposable(@NonNull Disposable disposable) {
        compositeDisposable.add(disposable);
    }
}
